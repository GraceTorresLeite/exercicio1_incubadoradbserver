# Exercicio1_IncubadoraDBServer

## Revisão 

### Exercícios 

**1) Estude elabore resumos dos seguintes tópicos:** 

* 1a) Ferramentas: 
    * bash - bash terminal reescrito- bourne again shell;
    * git - peça fundamental nas releases, tag é a marcação;
    * maven - gerência de dependências - bibliotecas / mvn packate- chama o jar/ mvn compile chama o javac;
    * Docker - computador /servidor :gerenciador de instalações- ferramenta(implantação de máquina de testes);
    * curl - localizador de recursos que permite get e post sem linha de comando)/ resposta em json; 
    * java - linguagem de programação;
    * javac - compilador java;
    * javadoc - gerador de documentação;
    * jar - arquivo compactado em java;
    * GitHub - plataforma de hospedagem de código-fonte com controle de versão usando o Git;
    * GitLab - O GitLab é um gerenciador de repositório de software baseado em git, com suporte a Wiki, issues(gerência de projetos);
    * Eclipse, IDE para desenvolvimento java;
    * STS - O Sprint Tool Suite (STS) é um toolbox baseado no Eclipse para desenvolvimento com Spring, que contém funcionalidades como o Boot Dashboard, suporte a todo o ecossistema do Spring e integração nativa com Maven, Gradle and Git;
    * Postman, API Client que facilita aos desenvolvedores criar, compartilhar, testar e documentar APIs. trabalha somente na internet(faz qualquer verbo);
    * Chrome  - internet + renderização componente motor , desenha o html rapidamente na tela;


* 1b) Bibliotecas: 
    * JDK - e é um conjunto de utilitários em java ;
    * Java EE - é uma plataforma de programação para servidores na linguagem de programação Java. @ Entity;
    * JUnit - framework com suporte à criação de testes automatizados na linguagem de programação Java. @Test/ Assert;
    * Spring Boot - O Spring Boot é um framework que permite ter uma aplicação rodando em produção rapidamente;
    * Spring Web - spring-boot-starter-web é utilizado para desenvolver serviços da web RESTful usando Spring MVC e Tomcat como contêiner do aplicativo  - 
@RequestParam - ?- obtenha um valor
@SpringBootAplicattion
@Controller
@RestController
@GetMapping
    * Thymeleaf -  é um mecanismo de modelo Java XML / XHTML / HTML5 que pode funcionar em ambientes web e não web;
th:text
th:each
th:object
th:replace
${} model attribute
*{object and each attribute} lista de pets dentro do each busca no contexto do thymeleaf
@{}resource URL
#{properties key} internacionalizar/ 
 ~{}indicando um elemento pois é substituição/ elemento busca e troca
*Reference card*
*cheat sheet de thymeleaf*


* 1c) Web: 
    * URL - O Uniform Resource Locator, é um termo técnico que foi traduzido para a língua portuguesa como "localizador uniforme de recursos";
    * HTML - HTML é uma linguagem de marcação utilizada na construção de páginas na Web;
    * HTTP - O Hypertext Transfer Protocol, sigla HTTP é um protocolo de comunicação utilizado para sistemas de informação de hipermídia, distribuídos e colaborativos.

* 1d) Arquiteturas: 
    * Model-View-Controller  - MVC é o acrônimo de Model-View-Controller é um padrão de projeto de software, ou padrão de arquitetura de software formulado na década de 1970, focado no reuso de código e a separação de conceitos em três camadas;
    * Layered/Layers - conceito de camadas;
    * Tiers Repository-  A camada do repositório inclui outros armazenamentos de dados. Alguns desses armazenamentos de dados podem ser referidos como bancos de dados ou repositórios em toda a documentação, com base em convenções de nomenclatura herdadas;
    * Entity - é uma coisa, concreta ou abstrata, incluindo associações entre entidades, abstraídos do mundo real e modelado em forma de tabela que guardarão informações no banco de dados;

* 1e) Pessoas:  🎓
    * Brian Fox, - criador do bash;
    * Ken Thompson - tb criou bash;
    * Linus Torvalds - git e linux;
    * Erich Gamma - autor de design patterns;
    * Kent Beck - TDD - Extreme programing/Junit;
    * Tim Berners-Lee - É o criador da World Wide Web;
    * Roy Fielding - pioneiro na www e Rest;
    * James Gosling - pai da linguagem Java;
    * Rod Johnson - criou o Spring Framework e co-fundou o SpringSource;
    * Daniel Fernández - spring e thymeleaf;

Localize informações em manuais técnicos e a Wikipédia. Anote ao menos uma fonte confiável para cada tópico. Use mapas mentais, anotações da Cornell e técnicas de estudo similares. 

**2) Desenvolva código para listar PetType e Vet Specialty:**

- [x] 2a) Hospede a PetClinic no Gitlab (git, STS). ✔
- [x] 2b) Registre e gerencie ordens de serviço (issues, GitLab) necessárias.  ✔
- [x] 2c) Adicione os controladores (controller, Spring), rotas necessárias.  ✔
- [x] 2d) Adicione listas nos controladores para simular dados necessários.  ✔
- [x] 2e) Adicione as vistas (view, Thymeleaf) necessárias.  ✔
- [ ] 2f) [EXTRA] Adicione testes para o código desenvolvido. (PENDENTE)

_O desenvolvimento da listagem de PetType está gravado em vídeo._

**3) Desenvolva código para listar Product:**

- [x] 3a) Localize até três atributos para um produto da veterinária  ✔
- [x] 3b) Registre e gerencie ordens de serviço (issues, GitLab) necessárias.  ✔
- [x] 3c) Adicione a classe Product  ✔
- [x] 3d) Adicione os controladores (controller, Spring), rotas necessárias.  ✔
- [x] 3e) Adicione listas nos controladores para simular dados necessários.  ✔
- [x] 3f) Adicione as vistas (view, Thymeleaf) necessárias.  ✔
- [ ] 3g) [EXTRA] Adicione testes para o código desenvolvido. (PENDENTE)

**4) [EXTRA] Acrescente localização para a Língua Portuguesa:**

- [x] 4a) Adicione um arquivo de mensagens. ✔
- [ ] 4b) Configure servidor ou aplicação para utilizar a pt ou pt_BR. (PENDENTE)


